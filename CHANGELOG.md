# Changelog
This changelog is inspired by [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## 0.2a2[20.03.2021]
### Fixed
- Documentation should now compile in read-the-docs.
- Setup configured correctly.

## 0.2a1 [20.03.2021]
- Release of alpha for further usage and testing in *dafungus*.