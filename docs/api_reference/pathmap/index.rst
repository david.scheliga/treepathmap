*******************
treepathmap.PathMap
*******************


.. autosummary::
   :toctree:
   :recursive:

   treepathmap.PathMap

==========
Properties
==========

.. autosummary::
   :toctree:

   treepathmap.PathMap.meta
   treepathmap.PathMap.tags
   treepathmap.PathMap.real_paths
   treepathmap.PathMap.selection_path_name
   treepathmap.PathMap.tree_node_items
   treepathmap.PathMap.tree_items

=======
Methods
=======

.. autosummary::
   :toctree:

   treepathmap.PathMap.is_empty
   treepathmap.PathMap.from_selection

===============================
Related to real paths (indexes)
===============================

.. autosummary::
   :toctree:

   treepathmap.PathMap.get_sub_paths_of_real_path
   treepathmap.PathMap.real_path_exists
   treepathmap.PathMap.get_indexes
   treepathmap.PathMap.selected_indexes

=========
Selecting
=========

.. autosummary::
   :toctree:

   treepathmap.PathMap.select
   treepathmap.PathMap.where

==============
Path map items
==============

.. autosummary::
   :toctree:

   treepathmap.PathMap.get_path_map_item_by_real_path
   treepathmap.PathMap.iter_rows
