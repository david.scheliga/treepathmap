*************
API reference
*************

.. autosummary::
   :toctree:

   treepathmap.map_tree
   treepathmap.wh_is


.. toctree::
  :maxdepth: 3
  :caption: Contents:

  pathmap/index