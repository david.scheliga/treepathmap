***********
treepathmap
***********

.. warning::
   The packages development (and its documentation) is in the **alpha** state.
   It was segregated from another package as being a stand alone package.
   Therefore major changes will during its further implementation into the
   targeted projects.

   Towards the beta (targeted release Q3/2021)

   .. include:: ../README.md
      :start-line: 34
      :end-line: 40

Introduction
------------

.. include:: ../README.md
   :start-line: 3
   :end-line: 16

.. image:: resources/treepathmap-icon.svg
   :height: 196px
   :width: 196px
   :alt: A map of a tree.
   :align: center


Installation
------------

Installing the latest release using pip is recommended.

::

   $ pip install treepathmap

The latest development state can be obtained from gitlab using pip.

::

   $ pip install git+https://gitlab.com/david.scheliga/treepathmap.git@dev


.. toctree::
   :maxdepth: 3
   :caption: Contents:

   basic_usage
   concept
   api_reference/index